<?php

namespace theloopcraft\msgowl;

use Illuminate\Support\ServiceProvider;

class MsgOwlServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/msgowl.php' => config_path('msgowl.php')
        ]);
    }   

    public function register()
    {
        // $this->app->singleton(MsgOwl::class, function(){
        //     return new MsgOwl();
        // });
    }

}