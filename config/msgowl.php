<?php

return [

    /*
    |--------------------------------------------------------------------------
    | TPS
    |--------------------------------------------------------------------------
    | requests => number of requests to keep in check with in the given seconds
    | seconds => number of seconds to keep the given requests constrained with in
    |
    */

    'tps' => [
        'requests' => env('MSG_OWL_REQUESTS', 200),
        'seconds' => env('MSG_OWL_SECONDS', 60),
    ],


    /*
    |--------------------------------------------------------------------------
    | Keys
    |--------------------------------------------------------------------------
    | rest_key => REST API key from msg owl
    | otp_key => OTP API key from msg owl
    |
    */
    'keys' => [
        'rest_key' =>  env('MSG_OWL_REST_KEY'),
        'otp_key' => env('MSG_OWL_OTP_KEY'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Notification
    |--------------------------------------------------------------------------
    | active => if balance notification sms are to be sent
    | threshold => threshold to send sms
    | contact_number => contact number to send sms notifications
    |
    */
    'notification' => [
        'active' => env('MSG_OWL_NOTIFICATION_ACTIVE',false),
        'threshold' => 10,
        'contact_number' => env('MSG_OWL_SMS_NUMBER'),
        'sms_msg' => env('MSG_OWL_SMS_MSG', 'Your MsgOwl balance is getting low'),
    ]
];